var a = 5;
//console.log(Math.floor(4, 5));
//console.log(Math.max(4, 5));

var b = 'string';
//console.log(b.charAt(0) + b.charAt(b.length-1));

// console.log(10 + 10);
// console.log(10 + '10');
// console.log('10' + 10);
// console.log('1' + 10 - '1');

var c = '100';
//console.log(typeof(parseInt(c)));

function maxNumber (a, b) {
    return Math.max(a, b);
}
//console.log(maxNumber(1, 2));

var funcA = function () {
    console.log('lambda func');
};
//funcA();

var funcB = function a () {
    console.log('a func in B');
};
//funcB();

function funcC () {
    return 'funcC fun';
}
//console.log(funcC());

// function () {
//     console.log('anon func works after read!');
// }();

(function a () {
    // body...
})();

// Tasks

function maxOfThree (a, b, c) {
    return Math.max(a, b, c);
}
//console.log(maxOfThree(1, 2, 3));

function isVowel (data) {
    var vowels = new Array('a', 'e', 'i', 'o', 'u');

    if (data.length == 1) {

        for (var i = 0; i < vowels.length; i++) {
            if (data === vowels[i]) {
                return true;
            }
        }
        return false;
    }
}
//console.log(isVowel('f'));

function translate (string) {

    var vowels = new Array('a', 'e', 'i', 'o', 'u');
    var result = [];

    string.split('').forEach(function (chare) {

        if (vowels.indexOf(chare) === 0) {
            chare = chare + 'o' + chare;
        }
        result.push(chare);
    });
    console.log(result.join(''));
}
translate('sadasde sdasd');